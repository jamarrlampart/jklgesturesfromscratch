#pragma once

#include "ofMain.h"
#include <vector>
#include "MovableObject.h"

class ofApp : public ofBaseApp
{
public:
    void setup() override;
    void update() override;
    void draw() override;
    void exit() override;
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    bool isInsideObject(MovableObject& object);
    
    // Number of objects in the screen
    const int numMovableObj = 4;
    
private:
    std::vector<MovableObject> objects;
    const int dimensions = MovableObject::dimensions;
    ofTrueTypeFont verdana;
    
    bool allLocked;
    bool isHighlighted;
    
    // Creates a highlight around the object
    ofRectangle *highlightBox;
    
    // MovableObject::allLocked = true;
};
