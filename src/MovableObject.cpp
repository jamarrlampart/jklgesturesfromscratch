//
//  MovableObject.cpp
//  Jajan_Gestural_Recognition
//
//  Created by Jajan on 3/26/15.
//
//

#include "MovableObject.h"

bool MovableObject::allLocked = true;

MovableObject::MovableObject()
{
    x = 100;
    y = 100;
    size = 50;
    color = ofColor(145, 34, 100);
}

MovableObject::MovableObject(double xPos, double yPos, int dimension, ofColor _color)
{
    x = xPos;
    y = yPos;
    size = dimension;
    color = _color;
}

void MovableObject::draw()
{
    ofSetColor(color);
    ofFill();
    ofRect(x, y, size, size);
    ofNoFill();
}

void MovableObject::setXPos(double xPos)
{
    x += (xPos - (x+dimensions/2))*0.5;
    // x += (xPos - x)*0.5;
    // x -= x/2*0.2;
}

void MovableObject::setYPos(double yPos)
{
    y += (yPos - (y+dimensions/2))*0.5;
}

double MovableObject::getXPos()
{
    return x;
}

double MovableObject::getYPos()
{
    return y;
}

void MovableObject::setIsMoving(bool moving)
{
    isMoving = moving;
}

bool MovableObject::getIsMoving()
{
    return isMoving;
}
