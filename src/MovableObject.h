//
//  MovableObject.h
//  Jajan_Gestural_Recognition
//
//  Created by Jajan on 3/26/15.
//
//

#ifndef Jajan_Gestural_Recognition_MovableObject_h
#define Jajan_Gestural_Recognition_MovableObject_h

#include <stdio.h>
#include "ofMain.h"

class MovableObject
{
private:
    double x,y;
    
    // false if not moving, true if highlighted or is moving
    bool isMoving = false;
    
    int size;
    ofColor color;
    
public:
    MovableObject();
    MovableObject(double xPos, double yPos, int dimension, ofColor color);
    void draw();
    
    void setXPos(double xPos);
    void setYPos(double yPos);
    void setIsMoving(bool moving);
    
    double getXPos();
    double getYPos();
    bool getIsMoving();
    
    // Size of object
    const static int dimensions = 150;
    
    // Keeps track of whether all objects in the screen are locked into place (i.e. cannot be moved)
    static bool allLocked;
     
};


#endif
