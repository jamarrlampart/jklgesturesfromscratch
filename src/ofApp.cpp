#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetLogLevel(OF_LOG_VERBOSE);
    
    for (int numObj = 0; numObj < numMovableObj; numObj++)
    {
        objects.push_back(MovableObject(200*numObj, 100*numObj, dimensions, ofColor(34*numObj, 20*numObj, 100)));
    }
    
    allLocked = true;
}

//--------------------------------------------------------------
void ofApp::update()
{
    if (allLocked) // none is moving
    {
        for (int index = objects.size()-1; index >= 0; index--)
        {
            if (isInsideObject(objects[index]))
            {
                if (ofGetMousePressed())
                {
                    allLocked = false;
                    objects[index].setIsMoving(true);
                    highlightBox = new ofRectangle();
                    break;
                }
                else
                {
                    highlightBox = new ofRectangle(objects[index].getXPos()-1, objects[index].getYPos()-1, dimensions + 2, dimensions + 2);
                    break;
                }
            }
            highlightBox = new ofRectangle();
        }
    }
    else // one is moving
    {
        if (!ofGetMousePressed())
        {
            allLocked = true;
            for (auto& object : objects)
            {
                if (object.getIsMoving())
                {
                    object.setIsMoving(false);
                    break;
                }
            }
        }
        
        else
        {
            for (auto& object : objects)
            {
                if (object.getIsMoving())
                {
                    object.setXPos(ofGetMouseX());
                    object.setYPos(ofGetMouseY());
                    break;
                }
            }
        }
    }
    
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofGetFrameRate();
    
    for (auto object : objects)
    {
        object.draw();
        ofSetColor(0,0,0);
        ofNoFill();
        ofRect(highlightBox->getX(), highlightBox->getY(), highlightBox->getWidth(), highlightBox->getHeight());
        ofFill();
    }
}

bool ofApp::isInsideObject(MovableObject& object)
{
    return (ofGetMouseX() >= object.getXPos() && ofGetMouseX() <= object.getXPos()+dimensions && ofGetMouseY() >= object.getYPos() && ofGetMouseY() <= object.getYPos()+dimensions);
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ) {
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------
void ofApp::exit()
{

}
